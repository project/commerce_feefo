<?php

/**
 * @file
 * Hooks provided by the Commerce Feefo module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows altering of individual order item lines added to Feefo data
 *
 * @param $data
 * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation
 * @param \Drupal\commerce_order\Entity\OrderItemInterface $orderItem
 */
function hook_commerce_feefo_product_data_alter($data, \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation, \Drupal\commerce_order\Entity\OrderItemInterface $orderItem) {

}

/**
 * Allows ordering of the whole Feefo data array
 *
 * @param $data
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 */
function hook_commerce_feefo_order_data_alter($data, \Drupal\commerce_order\Entity\OrderInterface $order) {

}

/**
 * @} End of "addtogroup hooks".
 */
