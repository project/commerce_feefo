# Overview

## Supported functions:
- Display a review summary block
- Display a review widget based upon the product code or parent product code
- Send order data to Feefo via the JS API (see here - https://support.feefo.com/support/solutions/articles/8000042145-javascript-sale-integration).

## Reviews summary Block
- Configure it with your Feefo Merchant ID
- Add a URL that clicking the badge will link to. Default to the page on Feefo, but could be your own site

## Feefo review listings block
- Choose the review mode
- Choose the field on the product which contains the product or parent product code

## Checkout Pane Setup
1. Configure the Feefo checkout pane in the order flow
2. Fill out all confifugration
3. Field mappings support tokens
4. Module hooks can be used to alter the product data and final Feefo data

## Gotchas
- When mapping categories, if you are using taxonomy terms, the token would be something like this - [commerce_product:field_category:0:entity:name].
- Note the 0:entity:name part. Without this you may end up with a URL.

## Module Hooks

There are two module hook available: -
- hook_commerce_feefo_product_data_alter() - alter order item line data going into the Feefo product data field
- hook_commerce_feefo_order_data_alter() - alter the final data array passed to the iFrame

## Contributions

Contributions are most welcome. Please submit and ticket with patches / forks :-)

## Credits
Initial development of this module is from <a href="https://www.drupology.com">Drupology</a>.
