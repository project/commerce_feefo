<?php

namespace Drupal\commerce_feefo;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\profile\Entity\Profile;
use Drupal\token\TokenInterface;

class Feefo {

  public const DEFAULT_REVIEWS_SUMMARY_URL = 'https://www.feefo.com/en-GB/reviews/[merchant_identifier]?displayFeedbackType=BOTH&amp;timeFrame=YEAR';

  /** @var ModuleHandlerInterface */
  protected $moduleHandler;

  /** @var \Drupal\Core\Utility\Token  */
  protected $token;

  /** @var array */
  protected $configuration;

  /** @var string */
  protected $merchantIdentifier;

  /** @var array */
  protected $order_item_bundles;

  /** @var int */
  protected $feedbackDays;

  /** @var array */
  protected $product_fields;

  /** @var OrderInterface */
  protected $order;

  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
    $this->token = \Drupal::getContainer()->get('token');
  }

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return \Drupal\commerce_feefo\Feefo
   */
  public function setOrder(?OrderInterface $order) {
    $this->order = $order;
    return $this;
  }

  /**
   * @return string
   */
  public function getMerchantIdentifier() {
    return $this->merchantIdentifier;
  }

  /**
   * @return array
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * @param array $config
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * @param string $merchantIdentifier
   *
   * @return \Drupal\commerce_feefo\Feefo
   */
  public function setMerchantIdentifier(string $merchantIdentifier) {
    $this->merchantIdentifier = $merchantIdentifier;
    return $this;
  }

  /**
   * @return array
   */
  public function getOrderItemBundles() {
    return $this->order_item_bundles;
  }

  /**
   * @param array $order_item_bundles
   *
   * @return \Drupal\commerce_feefo\Feefo
   */
  public function setOrderItemBundles(array $order_item_bundles) {
    $this->order_item_bundles = $order_item_bundles;
    return $this;
  }

  /**
   * @return int
   */
  public function getFeedbackDays() {
    return $this->feedbackDays;
  }

  /**
   * @param int $feedbackDays
   *
   * @return \Drupal\commerce_feefo\Feefo
   */
  public function setFeedbackDays(int $feedbackDays) {
    $this->feedbackDays = $feedbackDays;
    return $this;
  }

  /**
   * @return array
   */
  public function getProductFields() {
    return $this->product_fields;
  }

  /**
   * @param array $product_fields
   *
   * @return \Drupal\commerce_feefo\Feefo
   */
  public function setProductFields(array $product_fields) {
    $this->product_fields = $product_fields;
    return $this;
  }

  /**
   * @return array
   */
  function getFeefoData() {
    // check we have a license key
    if (empty($this->merchantIdentifier)) {
      throw new FeefoException('No merchant identifier provided');
    }

    if (empty($this->order_item_bundles)) {
      throw new FeefoException('No order item bundles specified');
    }

    // add skeleton data and call the hook for custom modules to full
    foreach($this->order->getItems() as $orderItem) {
      $orderItemType = $orderItem->get('type')->target_id;
      if (in_array($orderItemType, $this->order_item_bundles)) {
        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation */
        $productVariation = $orderItem->getPurchasedEntity();
        $product_datum = $this->product_fields;
        $entities = [
          'user' => $this->order->getCustomer(),
          'commerce_order' => $this->order,
          'commerce_order_item' => $orderItem,
          'commerce_product' => $productVariation->getProduct(),
          'commerce_product_variation' => $productVariation,
        ];
        $this->replaceTokens($product_datum, $entities);
        $this->moduleHandler->alter('commerce_feefo_product_data', $product_datum, $productVariation, $orderItem);
        $product_data[] = $product_datum;
      }
    }

    $feedback_date = strtotime("+" . $this->feedbackDays . " day");
    $feedback_date = date('d/m/Y', $feedback_date);

    $billingProfile = Profile::load($this->order->get('billing_profile')->target_id);
    $data = [
      'query' => [
        'merchantidentifier' => $this->merchantIdentifier,
      ],
      'attributes' => [
        'data-feefo-email' => $this->order->getEmail(),
        'data-feefo-orderref' =>  $this->order->getOrderNumber(),
        'data-feefo-name' => $billingProfile->get('address')->given_name . ' ' . $billingProfile->get('address')->family_name,
        'data-feefo-customerref' => $this->order->getCustomer()->id(),
        'data-feefo-feedbackdate' => $feedback_date,
        'data-feefo-products' => $product_data,
      ],
    ];
    $this->moduleHandler->alter('commerce_feefo_order_data', $data, $this->order);
    $data['attributes']['data-feefo-products'] = \GuzzleHttp\json_encode($data['attributes']['data-feefo-products']);

    return $data;
  }

  /**
   * @param $array
   * @param array $entities
   */
  public function replaceTokens(&$array, array $entities) {
    foreach($array as &$value) {
      if (is_array($value)) {
        $this->replaceTokens($value, $entities);
      }
      else {
        $cache = new BubbleableMetadata();
        $value = $this->token->replace($value, $entities, ['clear' => FALSE], $cache);
      }
    }
  }

}
