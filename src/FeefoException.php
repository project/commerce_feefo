<?php

namespace Drupal\commerce_feefo;

/**
 * Class FeefoException
 *
 * @package Drupal\commerce_feefo
 */
class FeefoException extends \RuntimeException {
}
