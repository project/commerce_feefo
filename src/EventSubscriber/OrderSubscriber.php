<?php

namespace Drupal\commerce_feefo\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderSubscriber implements EventSubscriberInterface {

  public function __construct() {

  }

  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['onPlacePostTransition', 0],
    ];
  }

  /**
   * Check if we need a gift card notification
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   */
  public function onPlacePostTransition(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

  }
}
