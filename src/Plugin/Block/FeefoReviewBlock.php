<?php

namespace Drupal\commerce_feefo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FeefoReviewBlock' block.
 *
 * @Block(
 *  id = "feefo_review_block",
 *  admin_label = @Translation("Feefo review listings"),
 * )
 */
class FeefoReviewBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\commerce_feefo\Feefo definition.
   *
   * @var \Drupal\commerce_feefo\Feefo
   */
  protected $commerceFeefoFeefo;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->commerceFeefoFeefo = $container->get('commerce_feefo.feefo');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * @return array|string[]
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'review_mode' => 'data-product-sku',
      'feefo_product_field' => '',
    ];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $options = [
      'data-product-sku' => $this->t('Product code'),
      'data-parent-product-sku' => $this->t('Parent product code'),
    ];
    $form['review_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Review mode'),
      '#options' => $options,
      '#default_value' => $this->configuration['review_mode'],
      '#required' => TRUE,
    ];
    $form['feefo_product_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field with Feefo SKU'),
      '#default_value' => $this->configuration['feefo_product_field'],
      '#required' => TRUE,
    ];
    $token_types = [
      'commerce_product',
    ];
    $form['tokens'] = \Drupal::service('token.tree_builder')->buildRenderable($token_types);
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $this->configuration['review_mode'] = $form_state->getValue('review_mode');
      $this->configuration['feefo_product_field'] = $form_state->getValue('feefo_product_field');
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // get product ID
    $product = \Drupal::routeMatch()->getParameter('commerce_product');
    if (empty($product)) {
      return [];
    }

    // replace the tokens in the config form
    /** @var \Drupal\commerce_feefo\Feefo $feefo */
    $feefo = \Drupal::getContainer()->get('commerce_feefo.feefo');
    $entities = [
      'commerce_product' => $product,
    ];
    $feefoSkuField = $this->configuration['feefo_product_field'];
    $replacements = [
      'sku_field' => $feefoSkuField,
    ];
    $feefo->replaceTokens($replacements, $entities);

    // allow other modules to alter the sku field
    $this->moduleHandler->alter('commerce_feefo_review_sku', $replacements, $product);

    if (!empty($replacements['sku_field'])) {
      // attach JS library
      $build['#attached']['library'][] = 'commerce_feefo/feefo_review_widget';
      $review_mode = $this->configuration['review_mode'];
      $build['feefo_review_placeholder'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            'feefo-review-widget-product',
          ],
          'id' => 'feefo-product-review-widgetId',
          $review_mode => $replacements['sku_field'],
        ]
      ];
    }

    $build['#cache'] = [
      'contexts' => [
        'url.path',
        'url.query_args',
      ]
    ];

    return $build;
  }

}
