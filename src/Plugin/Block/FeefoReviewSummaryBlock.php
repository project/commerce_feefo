<?php

namespace Drupal\commerce_feefo\Plugin\Block;

use Drupal\commerce_feefo\Feefo;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FeefoReviewBlock' block.
 *
 * @Block(
 *  id = "feefo_review_summary_block",
 *  admin_label = @Translation("Feefo review summary"),
 * )
 */
class FeefoReviewSummaryBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\commerce_feefo\Feefo definition.
   *
   * @var \Drupal\commerce_feefo\Feefo
   */
  protected $commerceFeefoFeefo;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->commerceFeefoFeefo = $container->get('commerce_feefo.feefo');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * @return array|string[]
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'merchant_identifier' => '',
      'reviews_url' => Feefo::DEFAULT_REVIEWS_SUMMARY_URL,
    ];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant identifier'),
      '#default_value' => $this->configuration['merchant_identifier'],
      '#required' => TRUE,
    ];
    $form['reviews_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reviews overview URL'),
      '#default_value' => $this->configuration['reviews_url'],
      '#maxlength' => 2048,
      '#required' => TRUE,
      '#description' => $this->t('You can enter an internal path or an external URL. [merchant_identifier] can be used as a pseudo token'),
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $this->configuration['merchant_identifier'] = $form_state->getValue('merchant_identifier');
      $this->configuration['reviews_url'] = $form_state->getValue('reviews_url');
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (empty($this->configuration['merchant_identifier'])) {
      return [];
    }
    $build['feefo_review_summary'] = [
      '#theme' => 'feefo_review_summary',
      '#merchant_identifier' => $this->configuration['merchant_identifier'],
      '#reviews_url' => $this->configuration['reviews_url'],
    ];
    return $build;
  }

}
