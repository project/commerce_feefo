<?php

namespace Drupal\commerce_feefo\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_feefo\Feefo;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the completion message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "feefo_send_order",
 *   label = @Translation("Send order to Feefo"),
 *   default_step = "complete",
 * )
 */
class FeefoSendOrder extends CheckoutPaneBase {

  /** @var \Drupal\commerce_feefo\Feefo */
  protected $feefo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('commerce_feefo.feefo')
    );
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, Feefo $feefo) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $feefo->setOrder($this->order);
    $bundles = $this->configuration['order_item_bundles'];
    foreach($bundles as $bundle_id => $bundle) {
      if (!empty($bundle)) {
        $order_item_bundles[] = $bundle_id;
      }
    }
    $feefo->setOrderItemBundles($order_item_bundles)
      ->setMerchantIdentifier($this->configuration['merchant_identifier'])
      ->setFeedbackDays((int) $this->configuration['feedback_days'])
      ->setProductFields($this->configuration['product_fields'])
    ;
    $this->feefo = $feefo;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_identifier' => '',
      'order_item_bundles' => [
        'product' => 'product',
      ],
      'feedback_days' => '7',
      'product_fields' => [
        'description' => '',
        'productsearchcode' => '',
        'tags' => [
          'saleschannel' => '',
          'category' => '',
        ]
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('commerce_order_item');
    foreach($bundles as $bundle_id => $bundle) {
      $options[$bundle_id] = $bundle['label'];
    }
    $form['merchant_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant identifier'),
      '#default_value' => $this->configuration['merchant_identifier'],
      '#required' => TRUE,
    ];
    $form['order_item_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Order item types to send to Feefo'),
      '#options' => $options,
      '#default_value' => $this->configuration['order_item_bundles'],
    ];
    $form['feedback_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of days before feedback emails are sent'),
      '#min' => 0,
      '#max' => 365,
      '#default_value' => $this->configuration['feedback_days'],
    ];
    $form['product_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Feefo product field mappings'),
      '#open' => TRUE,
    ];
    $form['product_fields']['description'] = [
      '#type' => 'textfield',
      '#title' => 'description',
      '#default_value' => $this->configuration['product_fields']['description'],
    ];
    $form['product_fields']['productsearchcode'] = [
      '#type' => 'textfield',
      '#title' => 'productsearchcode',
      '#default_value' => $this->configuration['product_fields']['productsearchcode'],
    ];
    $form['product_fields']['tags']['saleschannel'] = [
      '#type' => 'textfield',
      '#title' => 'saleschannel',
      '#default_value' => $this->configuration['product_fields']['tags']['saleschannel'],
    ];
    $form['product_fields']['tags']['category'] = [
      '#type' => 'textfield',
      '#title' => 'category',
      '#default_value' => $this->configuration['product_fields']['tags']['category'],
    ];
    $token_types = [
      'user',
      'commerce_order',
      'commerce_order_item',
      'commerce_product',
      'commerce_product_variation',
    ];
    $form['product_fields']['tokens'] = \Drupal::service('token.tree_builder')->buildRenderable($token_types);
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_identifier'] = $values['merchant_identifier'];
      $this->configuration['order_item_bundles'] = $values['order_item_bundles'];
      $this->configuration['feedback_days'] = $values['feedback_days'];
      unset($values['product_fields']['tokens']);
      $this->configuration['product_fields'] = $values['product_fields'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $summary = '';
    if (!empty($this->configuration['merchant_identifier'])) {
      $summary = $this->t('Merchant identifier: @id', ['@id' => $this->configuration['merchant_identifier']]) . '<br/>';
    }
    if (!empty($this->configuration['order_item_bundles'])) {
      foreach($this->configuration['order_item_bundles'] as $bundle) {
        if (!empty($bundle)) {
          $bundles[] = $bundle;
        }
      }
      $bundles = implode(',', $bundles);
      $summary .= $this->t('Bundles: @bundles', ['@bundles' => $bundles]) . '<br/>';
    }
    if (isset($this->configuration['feedback_days'])) {
      $summary .= $this->t('Feedback days: @days', ['@days' => $this->configuration['feedback_days']]) . '<br/>';
    }
    if (isset($this->configuration['feedback_days'])) {
      $summary .= $this->t('Feefo product field mappings:') . '<pre>' . $this->t('@fields', ['@fields' => \GuzzleHttp\json_encode($this->configuration['product_fields'], JSON_PRETTY_PRINT)]) . '<br/>';
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    try {
      $src = 'https://api.feefo.com/api/sales/merchant';
      $feefoOrder = $this->feefo->getFeefoData();
      if (empty($feefoOrder)) {
        return [];
      }
      $url_params = UrlHelper::buildQuery($feefoOrder['query']);

      // add the iFrame
      $pane_form['feefo_iframe'] = [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#attributes' => [
          'id' => 'FSI_IFrame',
          'class' => ['commerce_feefo_order_iframe'],
          'width' => 1,
          'height' => 1,
          'src' => $src . '?' . $url_params,
        ],
      ];
      foreach ($feefoOrder['attributes'] as $key => $value) {
        $pane_form['feefo_iframe']['#attributes'][$key] = $value;
      }
      $pane_form['#attached']['library'][] = 'commerce_feefo/feefo_post_message';
      $pane_form['#attached']['library'][] = 'commerce_feefo/commerce_feefo';
      return $pane_form;
    }
    catch (\Throwable $ex) {
      \Drupal::logger('commerce_feefo')->error($ex->getMessage());
      return [];
    }
  }

}
