(function($) {

  /**
   * Disable the continue buttons in the checkout process once they are clicked
   * and provide a notification to the user.
   */
  Drupal.behaviors.commerce_feefo_init = {
    attach: function (context, settings) {
      var FPMH = new Feefo.PostMessageHandler();
      FPMH.init();
    }
  }
})(jQuery);
